import argparse
import os
import pandas as pd
import pickle

from math import sqrt
from datetime import datetime
from pandas import DataFrame

from sklearn import metrics
from sklearn.linear_model import Ridge, LogisticRegression
from sklearn.preprocessing import (
	RobustScaler,
	StandardScaler,
	PolynomialFeatures,
	)

from scipy.stats.stats import pearsonr

from utils import transform_number_features
from utils import transform_datetime_features

from sklearn.ensemble import (
	AdaBoostClassifier,
	AdaBoostRegressor,
	BaggingClassifier,
	BaggingRegressor,
	ExtraTreesClassifier,
	ExtraTreesRegressor,
	GradientBoostingClassifier,
	GradientBoostingRegressor,
	IsolationForest,
	RandomForestClassifier,
	RandomForestRegressor,
	RandomTreesEmbedding,
	VotingClassifier,
	)
from sklearn.linear_model import (
	ARDRegression,
	BayesianRidge,
	ElasticNet,
	ElasticNetCV,
	Hinge,
	Huber,
	HuberRegressor,
	Lars,
	LarsCV,
	Lasso,
	LassoCV,
	LassoLars,
	LassoLarsCV,
	LassoLarsIC,
	LinearRegression,
	Log,
	LogisticRegression,
	LogisticRegressionCV,
	ModifiedHuber,
	MultiTaskElasticNet,
	MultiTaskElasticNetCV,
	MultiTaskLasso,
	MultiTaskLassoCV,
	OrthogonalMatchingPursuit,
	OrthogonalMatchingPursuitCV,
	PassiveAggressiveClassifier,
	PassiveAggressiveRegressor,
	Perceptron,
	RandomizedLasso,
	RandomizedLogisticRegression,
	Ridge,
	RidgeCV,
	RidgeClassifier,
	RidgeClassifierCV,
	SGDClassifier,
	SGDRegressor,
	SquaredLoss,
	TheilSenRegressor,
	RANSACRegressor,
	)

import numpy as np
from sklearn.feature_selection import (
	GenericUnivariateSelect,
	RFE,
	RFECV,
	SelectFdr,
	SelectFpr,
	SelectFwe,
	SelectKBest,
	SelectFromModel,
	SelectPercentile,
	VarianceThreshold,
	chi2,
	f_classif,
	f_oneway,
	f_regression,
	mutual_info_classif,
	mutual_info_regression,
	)

# use this to stop the algorithm before time limit exceeds
TIME_LIMIT = int( os.environ.get( 'TIME_LIMIT', 5 * 60 ) )

ONEHOT_MAX_UNIQUE_VALUES = 20

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument( '--train-csv', type=argparse.FileType( 'r' ), required=True )
	parser.add_argument( '--model-dir', required=True )
	parser.add_argument( '--mode', choices=['classification', 'regression'], required=True )
	args = parser.parse_args()

	start_time = datetime.now()

	df_train = pd.read_csv( args.train_csv )
	df_train.drop_duplicates( inplace=True )
	df_train_y = df_train.target
	df_train_X = df_train.drop( 'target', axis=1 )

	df_test = pd.read_csv( os.path.join( os.path.dirname( args.train_csv.name ), 'test.csv' ) )
	df_target = pd.read_csv( os.path.join( os.path.dirname( args.train_csv.name ), 'test-target.csv' ) )

	print( 'Dataset test read, shape {}'.format( df_test.shape ) )
	print( 'Dataset train read, shape {}'.format( df_train_X.shape ) )

	# dict with data necessary to make predictions =============================
	model_config = {}
	number_proc_columns = model_config['number_proc_columns'] = {}

	# features from number ===================================================
	df_train_X = transform_number_features( df_train_X, number_proc_columns )
	print 0
	df_test = transform_number_features( df_test, number_proc_columns )
	print 0

	# features from datetime ===================================================
	df_test = transform_datetime_features( df_test )
	df_train_X = transform_datetime_features( df_train_X )
	print 1

	# missing values ===========================================================
	if any( df_train_X.isnull() ):
		model_config['missing'] = True
		df_train_X.fillna( -1, inplace=True )

	if any( df_test.isnull() ):
		df_test.fillna( value=df_test.mean( axis=0 ), inplace=True )
		df_test.fillna( -1, inplace=True )
	print 2

	# categorical encoding =====================================================
	categorical_values = {}
	for col_name in list( df_train_X.columns ):
		col_unique_values = df_train_X[col_name].unique()
		if 2 < len( col_unique_values ) <= ONEHOT_MAX_UNIQUE_VALUES:
			categorical_values[col_name] = col_unique_values
			for unique_value in col_unique_values:
				df_train_X['onehot_{}={}'.format( col_name, unique_value )] = (df_train_X[col_name] == unique_value).astype( int )
	model_config['categorical_values'] = categorical_values

	for col_name, unique_values in model_config['categorical_values'].items():
		for unique_value in unique_values:
			df_test['onehot_{}={}'.format( col_name, unique_value )] = ( df_test[col_name] == unique_value ).astype( int )

	print 3

	# drop constant features ===================================================
	constant_columns = [
		col_name
		for col_name in df_train_X.columns
		if df_train_X[col_name].nunique() == 1
		]
	df_train_X.drop( constant_columns, axis=1, inplace=True )

	# use only numeric columns ================================================
	used_columns = [
		col_name
		for col_name in df_train_X.columns
		if col_name.startswith( 'number' ) or col_name.startswith( 'onehot' )
		]
	df_train_X = df_train_X[used_columns]
	df_test = df_test[used_columns]

	print 4

	# Generate polynomial and interaction features ================================================
	number_columns = [
		col_name
		for col_name in df_train_X.columns
		if col_name.startswith( 'number' )
		]
	if 4096 < df_train_X.shape[0] < 65536 and len( number_columns ) < 64 and args.mode == 'regression':

		df_train_X_poly = df_train_X[ number_columns ]
		df_test_poly = df_test[ number_columns ]

		poly = PolynomialFeatures( 2 )
		poly.fit( df_train_X_poly, df_train_y )

		df_train_X_poly = DataFrame( poly.transform( df_train_X_poly ), columns=poly.get_feature_names( df_train_X_poly.columns ) )
		df_test_poly = DataFrame( poly.transform( df_test_poly ), columns=poly.get_feature_names( df_test_poly.columns ) )

		df_train_X[ df_train_X_poly.columns ] = df_train_X_poly
		df_test[ df_test_poly.columns ] = df_test_poly

	print 5

	# Feature selection =======================================================
	score_func = f_regression if args.mode == 'regression' else f_classif
	for sel in [
		# VarianceThreshold( threshold=0.1 ),
		# SelectKBest( chi2, k=2 ),
		# SelectFpr( score_func=score_func ),
		# SelectFdr( score_func=score_func ),
		SelectFwe( score_func=score_func ),
		# GenericUnivariateSelect( score_func=score_func ),
		]:
		sel.fit( df_train_X, df_train_y )
		used_columns = df_train_X.columns[ sel.get_support( indices=True ) ]
		df_train_X = df_train_X[ used_columns ]
		print used_columns

	if len( df_train_X.columns ) > 256:
		sel = SelectPercentile( score_func=score_func, percentile=100.0 * 256.0 / len( df_train_X.columns ) )
		sel.fit( df_train_X, df_train_y )
		used_columns = df_train_X.columns[sel.get_support( indices=True )]
		df_train_X = df_train_X[used_columns]

	used_columns = df_train_X.columns
	model_config['used_columns'] = used_columns

	# scaling ================================================================
	scaler = StandardScaler()
	# scaler = RobustScaler()
	df_train_X = scaler.fit_transform( df_train_X )
	model_config['scaler'] = scaler

	df_test = model_config['scaler'].transform( df_test[used_columns] )

	# fitting ================================================================
	model_config['mode'] = args.mode
	if args.mode == 'regression':
		model_clss = [
			# # ensemble
			# # AdaBoostRegressor,
			# BaggingRegressor,
			ExtraTreesRegressor,
			# GradientBoostingRegressor,
			# # IsolationForest,
			RandomForestRegressor,
			# # RandomTreesEmbedding,
			# # linear_model
			# # ARDRegression,
			# # BayesianRidge,
			# # ElasticNet,
			# # ElasticNetCV,
			# # Hinge,
			# # Huber,
			# # HuberRegressor,
			# # Lars,
			# # LarsCV,
			# # Lasso,
			# # LassoCV,
			# # LassoLars,
			# # LassoLarsCV,
			# # LassoLarsIC,
			# LinearRegression,
			# # Log,
			# # LogisticRegression,
			# # LogisticRegressionCV,
			# # ModifiedHuber,
			# # MultiTaskElasticNet,
			# # MultiTaskElasticNetCV,
			# # MultiTaskLasso,
			# # MultiTaskLassoCV,
			# # OrthogonalMatchingPursuit,
			# # OrthogonalMatchingPursuitCV,
			# # PassiveAggressiveRegressor,
			# # Perceptron,
			# # RandomizedLasso,
			# # RandomizedLogisticRegression,
			# Ridge,
			# RidgeCV,
			# # RidgeClassifierCV,
			# # SGDClassifier,
			# # SGDRegressor,
			# # SquaredLoss,
			# # TheilSenRegressor,
			# # RANSACRegressor,
			]
	else:
		model_clss = [
			# LogisticRegression,
			# LogisticRegressionCV,
			# PassiveAggressiveClassifier,
			# RidgeClassifier,
			# RidgeClassifierCV,
			# SGDClassifier,
			# AdaBoostClassifier,
			BaggingClassifier,
			ExtraTreesClassifier,
			# GradientBoostingClassifier,
			RandomForestClassifier,
			# VotingClassifier,
			]

	top = []
	for cls in model_clss:
		print '=' * 32
		print cls.__name__
		fit_start = datetime.now()

		model = cls()
		for param, value in [
			( 'n_jobs', 2 ),
			( 'max_features', 128 if df_train_X.shape[1] > 128 else df_train_X.shape[1] - 1 ),
			( 'max_samples', 65536 ),
			]:
			if param in cls.__init__.__code__.co_varnames:
				model = model.set_params( **{ param: value } )

		print 6
		model.fit( df_train_X, df_train_y )
		print 7
		if hasattr( model, 'feature_importances_' ):
			print sorted( zip( map( lambda x: round( x, 4 ), model.feature_importances_ ), used_columns ), reverse=True )
		model.score( df_train_X, df_train_y )
		print 8
		model_config['model'] = model

		d = {
			'name': cls.__name__,
			'fit_time': datetime.now() - fit_start,
			}

		df_test_Y = None
		if args.mode == 'regression':
			df_test_Y = pd.DataFrame()
			df_test_Y['Y'] = model.predict( df_test )

			target_min = np.percentile( df_train_y, 0.90 )
			target_max = np.percentile( df_train_y, 99.1 )

			df_test_Y['Y'] = df_test_Y['Y'].apply( lambda x: x if x > target_min else target_min )
			df_test_Y['Y'] = df_test_Y['Y'].apply( lambda x: x if x < target_max else target_max )

			df_test_Y = df_test_Y['Y']

		elif args.mode == 'classification':
			df_test_Y = model.predict_proba( df_test )[:, 1]
			# df_test_Y = model.predict( df_test )

		# show cls metrics ===================================================
		print
		if args.mode == 'regression':
			d['pearsonr'] = pearsonr( df_test_Y, df_target.target )[0]
			d['rmse'] = sqrt( metrics.mean_squared_error( df_target.target, df_test_Y ) )

			print 'pearsonr', d['pearsonr']
			print 'rmse', d['rmse']

		elif args.mode == 'classification':
			d['roc_auc_score'] = metrics.roc_auc_score( df_target.target, df_test_Y )
			print 'roc_auc_score', d['roc_auc_score']
		print

		top.append( d )

	# show top ===============================================================
	if args.mode == 'regression':
		for q in sorted(top, key=lambda x: x['pearsonr'] ):
			print '{rmse:10}\t{pearsonr:10}\t{fit_time}\t{name}'.format( **q )
		print '-'*16
		for q in sorted(top, key=lambda x: x['rmse'] ):
			print '{rmse:10}\t{pearsonr:10}\t{fit_time}\t{name}'.format( **q )

	elif args.mode == 'classification':
		for q in sorted(top, key=lambda x: x['roc_auc_score'] ):
			print '{roc_auc_score:10}\t{fit_time}\t{name}'.format( **q )

	# save trained model =====================================================
	model_config_filename = os.path.join( args.model_dir, 'model_config.pkl' )
	with open( model_config_filename, 'wb' ) as fout:
		pickle.dump( model_config, fout, protocol=pickle.HIGHEST_PROTOCOL )

	print( 'Train time: {}'.format( datetime.now() - start_time ) )
