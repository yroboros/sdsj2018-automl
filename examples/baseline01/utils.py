import datetime
import numpy as np


def parse_dt( x ):
	if not isinstance( x, str ):
		return None
	elif len( x ) == len( '2010-01-01' ):
		return datetime.datetime.strptime( x, '%Y-%m-%d' )
	elif len( x ) == len( '2010-01-01 10:10:10' ):
		return datetime.datetime.strptime( x, '%Y-%m-%d %H:%M:%S' )
	else:
		return None


def transform_datetime_features( df ):
	datetime_columns = [
		col_name
		for col_name in df.columns
		if col_name.startswith( 'datetime' )
		]
	for col_name in datetime_columns:
		df[col_name] = df[col_name].apply( lambda x: parse_dt( x ) )
		df['number_weekday_{}'.format( col_name )] = df[col_name].apply( lambda x: x.weekday() )
		df['number_month_{}'.format( col_name )] = df[col_name].apply( lambda x: x.month )
		df['number_day_{}'.format( col_name )] = df[col_name].apply( lambda x: x.day )
		df['number_hour_{}'.format( col_name )] = df[col_name].apply( lambda x: x.hour )
		df['number_hour_of_week_{}'.format( col_name )] = df[col_name].apply( lambda x: x.hour + x.weekday() * 24 )
		df['number_minute_of_day_{}'.format( col_name )] = df[col_name].apply( lambda x: x.minute + x.hour * 60 )
	return df


def transform_number_features( df, number_proc_columns, drop=False ):
	def proc_val( x, proc ):
		for n, p in enumerate( proc ):
			if x <= p:
				return n / float( len( proc ) )
		if x >= proc[-1]:
			return 0.99
		return -1

	number_columns = [
		col_name
		for col_name in df.columns
		if col_name.startswith( 'number' )
		]
	for col_name in number_columns:
		df[col_name] = df[col_name].astype( float )

	for col_name in number_columns[0:128]:
		proc = number_proc_columns.get( col_name, [ np.percentile( df[col_name], i ) for i in range( 1, 99, 5 ) ] )
		df['number_proc_{}'.format( col_name )] = df[col_name].apply( lambda x: proc_val( x, proc ) )
		if drop:
			df.drop( col_name, axis=1, inplace=True )

	return df
