import argparse
import os
import pandas as pd
import pickle
import time

from pandas import DataFrame

from utils import transform_number_features
from utils import transform_datetime_features

# use this to stop the algorithm before time limit exceeds
TIME_LIMIT = int( os.environ.get( 'TIME_LIMIT', 5 * 60 ) )

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument( '--test-csv', type=argparse.FileType( 'r' ), required=True )
	parser.add_argument( '--prediction-csv', type=argparse.FileType( 'w' ), required=True )
	parser.add_argument( '--model-dir', required=True )
	args = parser.parse_args()

	start_time = time.time()

	# load model
	model_config_filename = os.path.join( args.model_dir, 'model_config.pkl' )
	with open( model_config_filename, 'rb' ) as fin:
		model_config = pickle.load( fin )

	# read dataset
	df = pd.read_csv( args.test_csv )
	print('Dataset read, shape {}'.format( df.shape ))

	# features from datetime
	df = transform_number_features( df, model_config['number_proc_columns'], drop=(model_config['mode'] == 'classification') )
	df = transform_datetime_features( df )

	# missing values
	if any( df.isnull() ):
		df.fillna( value=df.mean( axis=0 ), inplace=True )
		df.fillna( -1, inplace=True )

	# categorical encoding
	for col_name, unique_values in model_config['categorical_values'].items():
		for unique_value in unique_values:
			df['onehot_{}={}'.format( col_name, unique_value )] = (df[col_name] == unique_value).astype( int )

	# Generate polynomial and interaction features ================================================
	poly = model_config.get( 'poly' )
	if poly:
		number_columns = [
			col_name
			for col_name in df.columns
			if col_name.startswith( 'number' )
			]
		df_test_poly = df[number_columns]
		df_test_poly = DataFrame( poly.transform( df_test_poly ), columns=poly.get_feature_names( df_test_poly.columns ) )
		df[df_test_poly.columns] = df_test_poly

	# filter columns
	used_columns = model_config['used_columns']

	# scale
	X_scaled = model_config['scaler'].transform( df[used_columns] )

	model = model_config['model']
	if model_config['mode'] == 'regression':
		df['prediction'] = model.predict( X_scaled )
	elif model_config['mode'] == 'classification':
		df['prediction'] = model.predict_proba( X_scaled )[:, 1]

	df[['line_id', 'prediction']].to_csv( args.prediction_csv, index=False )

	print('Prediction time: {}'.format( time.time() - start_time ))
