import argparse
import os
import pandas as pd
import pickle

from datetime import datetime
from pandas import DataFrame

from sklearn.linear_model import Ridge, LogisticRegression
from sklearn.preprocessing import (
	RobustScaler,
	StandardScaler,
	PolynomialFeatures,
	)

from utils import transform_number_features
from utils import transform_datetime_features

from sklearn.ensemble import (
	BaggingClassifier,
	BaggingRegressor,
	ExtraTreesClassifier,
	ExtraTreesRegressor,
	RandomForestClassifier,
	RandomForestRegressor,
	)

from sklearn.feature_selection import (
	SelectFwe,
	SelectPercentile,
	f_classif,
	f_regression,
	)

# use this to stop the algorithm before time limit exceeds
TIME_LIMIT = int( os.environ.get( 'TIME_LIMIT', 5 * 60 ) )

ONEHOT_MAX_UNIQUE_VALUES = 20

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument( '--train-csv', type=argparse.FileType( 'r' ), required=True )
	parser.add_argument( '--model-dir', required=True )
	parser.add_argument( '--mode', choices=['classification', 'regression'], required=True )
	args = parser.parse_args()

	start_time = datetime.now()

	df_train = pd.read_csv( args.train_csv )
	df_train.drop_duplicates( inplace=True )
	df_train_y = df_train.target
	df_train_X = df_train.drop( 'target', axis=1 )

	print( 'Dataset train read, shape {}'.format( df_train_X.shape ) )

	# dict with data necessary to make predictions =============================
	model_config = {}
	number_proc_columns = model_config['number_proc_columns'] = {}

	# features from number ===================================================
	df_train_X = transform_number_features( df_train_X, number_proc_columns, drop=(args.mode == 'classification') )

	# features from datetime ===================================================
	df_train_X = transform_datetime_features( df_train_X )

	# missing values ===========================================================
	if any( df_train_X.isnull() ):
		model_config['missing'] = True
		df_train_X.fillna( -1, inplace=True )

	# categorical encoding =====================================================
	categorical_values = {}
	for col_name in list( df_train_X.columns ):
		col_unique_values = df_train_X[col_name].unique()
		if 2 < len( col_unique_values ) <= ONEHOT_MAX_UNIQUE_VALUES:
			categorical_values[col_name] = col_unique_values
			for unique_value in col_unique_values:
				df_train_X['onehot_{}={}'.format( col_name, unique_value )] = (df_train_X[col_name] == unique_value).astype( int )
	model_config['categorical_values'] = categorical_values

	# drop constant features ===================================================
	constant_columns = [
		col_name
		for col_name in df_train_X.columns
		if df_train_X[col_name].nunique() == 1
		]
	df_train_X.drop( constant_columns, axis=1, inplace=True )

	# use only numeric columns ================================================
	used_columns = [
		col_name
		for col_name in df_train_X.columns
		if col_name.startswith( 'number' ) or col_name.startswith( 'onehot' )
		]
	df_train_X = df_train_X[used_columns]

	# # Generate polynomial and interaction features ================================================
	# number_columns = [
	# 	col_name
	# 	for col_name in df_train_X.columns
	# 	if col_name.startswith( 'number' )
	# 	]
	# if 4096 < df_train_X.shape[0] < 65536 and len( number_columns ) < 64 and args.mode == 'regression':
	#
	# 	df_train_X_poly = df_train_X[ number_columns ]
	#
	# 	poly = model_config['poly'] = PolynomialFeatures( 2 )
	# 	poly.fit( df_train_X_poly, df_train_y )
	#
	# 	df_train_X_poly = DataFrame( poly.transform( df_train_X_poly ), columns=poly.get_feature_names( df_train_X_poly.columns ) )
	#
	# 	df_train_X[ df_train_X_poly.columns ] = df_train_X_poly

	# Feature selection =======================================================
	score_func = f_regression if args.mode == 'regression' else f_classif
	sel = SelectFwe( score_func=score_func )
	sel.fit( df_train_X, df_train_y )
	used_columns = df_train_X.columns[sel.get_support( indices=True )]
	df_train_X = df_train_X[used_columns]

	if len( df_train_X.columns ) > 256:
		sel = SelectPercentile( score_func=score_func, percentile=100.0 * 256.0 / len( df_train_X.columns ) )
		sel.fit( df_train_X, df_train_y )
		used_columns = df_train_X.columns[sel.get_support( indices=True )]
		df_train_X = df_train_X[used_columns]

	used_columns = df_train_X.columns
	model_config['used_columns'] = used_columns

	# scaling ================================================================
	scaler = StandardScaler()
	df_train_X = scaler.fit_transform( df_train_X )
	model_config['scaler'] = scaler

	# fitting ================================================================
	model_config['mode'] = args.mode
	if args.mode == 'regression':
		model = RandomForestRegressor( n_jobs=4, max_features=128 if df_train_X.shape[1] > 128 else df_train_X.shape[1] - 1 )
	else:
		model = RandomForestClassifier( n_jobs=4, max_features=128 if df_train_X.shape[1] > 128 else df_train_X.shape[1] - 1 )

	model.fit( df_train_X, df_train_y )
	model.score( df_train_X, df_train_y )
	model_config['model'] = model

	# save trained model =====================================================
	model_config_filename = os.path.join( args.model_dir, 'model_config.pkl' )
	with open( model_config_filename, 'wb' ) as fout:
		pickle.dump( model_config, fout, protocol=pickle.HIGHEST_PROTOCOL )

	print( 'Train time: {}'.format( datetime.now() - start_time ) )
