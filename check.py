import os
import pickle
import argparse
import pandas as pd
from sklearn import metrics
from math import sqrt

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument( '--test-target-csv', type=argparse.FileType( 'r' ), required=True )
	parser.add_argument( '--prediction-csv', type=argparse.FileType( 'r' ), required=True )
	parser.add_argument( '--mode', choices=['classification', 'regression'], required=True )
	args = parser.parse_args()

	df_prediction = pd.read_csv( args.prediction_csv )
	df_test_target = pd.read_csv( args.test_target_csv )

	print
	if args.mode == 'regression':
		print 'rmse', sqrt( metrics.mean_squared_error( df_test_target.target, df_prediction.prediction ) )

	elif args.mode == 'classification':
		print 'roc_auc_score', metrics.roc_auc_score( df_test_target.target, df_prediction.prediction )
	print
